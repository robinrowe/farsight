# components/CMakeLists.txt
# Created by Robin Rowe 2022-08-01
# License: MIT Open Source

project(components)
message("Configuring ${PROJECT_NAME}...")

add_subdirectory(debughlp)
add_subdirectory(ownd)
#add_subdirectory(platform)