
# src/farsight/CMakeLists.txt
# Created by Robin Rowe 2022-08-01
# License: MIT Open Source

project(farsight)
message("Configuring ${PROJECT_NAME}...")

file(STRINGS sources.cmake SOURCES)
add_library(${PROJECT_NAME}_lib ${SOURCES})
link_libraries(${PROJECT_NAME}_lib)

add_executable(farsight WIN32 zoomplus.cpp zoomplus.rc)
target_link_libraries(farsight farsight_lib debughlp_lib ownd_lib)
