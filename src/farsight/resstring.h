/*----------------------------------------------------------------------
Copyright (c) 2000 Gipsysoft. All Rights Reserved.
File:	resstring.h
Owner:	russf@gipsysoft.com
Purpose:	Simple class to load a string from a resource table
----------------------------------------------------------------------*/
#ifndef RESSTRING_H
#define RESSTRING_H

#include <string>

class CResString
//
//	Simple class that loads a string from the resources. It deferrs loading of the
//	string until the string is actually asked for via the conversion operator.
{	static char* blank;
public:
	explicit CResString( UINT uStringID );
	virtual ~CResString();

	int GetLength() const;
	//	Give you access to the string
	char* c_str() const;
//	LPTSTR c_str();
private:
	CResString();
	UINT m_uStringID;
	mutable std::string s;
	const int s_size = 80;
//	mutable LPTSTR m_pcszString;
//	mutable int m_nLength;

	static HINSTANCE h;
	static HANDLE g_hHeap;
};


#endif //RESSTRING_H