# AR Glasses

* https://vrscout.com/news/build-your-own-open-source-ar-headset/
* https://www.instructables.com/Arduino-Glass-Open-Source-Augmented-Reality-Headse/
* https://wiki.opensourceecology.org/wiki/Open_Source_AR_Headset
* https://openar.fi/
* https://hypebeast.com/2022/2/apple-augmented-reality-realityos-open-source-code
* https://externlabs.com/blogs/open-source-platform-for-ar-and-vr/
* https://www.aliexpress.com/item/2261799887640284.html?gatewayAdapt=4itemAdapt
* https://dl.acm.org/doi/abs/10.1145/3460418.3479359
* https://www.relativty.com/
* https://www.goodfirms.co/augmented-reality-software/blog/best-free-open-source-augmented-reality-software
* https://blog.leapmotion.com/north-star-open-source/
* https://uploadvr.com/cr-deck-mk-1-open-source-ar-headset-project-north-star/
* https://www.smart-prototyping.com/AR-VR-MR-XR/AR-VR-Kits-Bundles
* https://hub.packtpub.com/leap-motion-open-sources-its-100-augmented-reality-headset-north-star/
* https://www.kura.tech/products/#pre-order
