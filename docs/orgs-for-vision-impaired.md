# Organizations for the Vision Impaired

* https://www.theguardian.com/us-news/2022/aug/09/ashton-kutcher-vasculitis-autoimmune-disease
* https://www.afb.org/blindness-and-low-vision/visionaware
* https://www.nvisioncenters.com/education/resources-for-visually-impaired/
* https://nfb.org/about-us/contact-us
* https://www.cvs.com/content/pharmacy/spoken-rx
* https://www.who.int/groups/dh-tag-membership
* https://www.who.int/news-room/fact-sheets/detail/blindness-and-visual-impairment
* https://www.who.int/health-topics/blindness-and-vision-loss#tab=tab_1
* https://www.who.int/publications/i/item/9789241516570
* https://www.iapb.org/connect/join-iapb/
* https://www.colourblindawareness.org/colour-blindness/types-of-colour-blindness/
