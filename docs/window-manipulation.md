# Window Manipulation

* https://github.com/BiGilSoft/WindowTop
* https://docs.microsoft.com/en-us/windows/win32/winmsg/extended-window-styles
* https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-setwindowrgn
* https://docs.microsoft.com/en-us/answers/questions/705880/how-to-create-a-window-without-background-or-39era.html
* https://stackoverflow.com/questions/3970066/creating-a-transparent-window-in-c-win32
* https://stackoverflow.com/questions/49380566/is-it-possible-to-create-a-winapi-window-with-only-borders
* https://docs.microsoft.com/en-us/windows/win32/gdi/window-background
* https://superkogito.github.io/blog/2020/07/26/capture_screen_using_gdiplus.html
* https://docs.microsoft.com/en-us/windows/win32/gdi/capturing-an-image
* https://www.codeproject.com/Articles/15925/Image-Magnifier-Control
* https://www.codeproject.com/Articles/4513/Making-of-a-Color-Spy-utility-with-WTL
* https://github.com/donaldnevermore/window-opacity
* https://github.com/alfredomtx/Transparent-Window-App

